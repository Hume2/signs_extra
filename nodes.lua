--[[
    signs_trains mod for Minetest - Various road signs with text displayed
    on.
    (c) Hume2
    (c) Pierre-Yves Rollo

    This file is part of signs_road.

    signs_extra is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    signs_extra is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with signs_road.  If not, see <http://www.gnu.org/licenses/>.
--]]

--local S = signs_road.intllib

local models = {
	speed = {
		depth = 1/16,
		width = 32/32,
		height = 22/32,
		entity_fields = {
			maxlines = 1,
			color = "#000",
			aspect_ratio = 2/3,
		},
		node_fields = {
		   visual_scale = 1,
			description = "Speed sign",
			tiles = { "signs_trains_side.png", "signs_trains_side.png",
			          "signs_trains_side.png", "signs_trains_side.png",
			          "signs_trains_side.png", "signs_trains_speed.png" },
			inventory_image = "signs_trains_speed_item.png",
		},
	},
	down = {
		depth = 1/16,
		width = 32/32,
		height = 32/32,
		entity_fields = {
			size = { x = 32/32, y = 24/32 },
			maxlines = 1,
			color = "#000",
		},
		node_fields = {
		   visual_scale = 1,
			description = "Speed forecast",
			tiles = { "signs_trains_nothing.png", "signs_trains_nothing.png",
			          "signs_trains_nothing.png", "signs_trains_nothing.png",
			          "signs_trains_down_side.png", "signs_trains_down.png" },
			inventory_image = "signs_trains_down_item.png",
		},
	},
	hectometer = {
		depth = 1/16,
		width = 16/32,
		height = 16/32,
		entity_fields = {
			maxlines = 2,
			color = "#000",
			aspect_ratio = 1,
		},
		node_fields = {
		   visual_scale = 1,
			description = "Hectometer sign",
			tiles = { "signs_trains_side.png", "signs_trains_side.png",
			          "signs_trains_side.png", "signs_trains_side.png",
			          "signs_trains_side.png", "signs_trains_hectometer_item.png" },
			inventory_image = "signs_trains_hectometer_item.png",
		},
	},
}

-- Node registration
for name, model in pairs(models)
do
	signs_api.register_sign("signs_trains", name, model)
end
